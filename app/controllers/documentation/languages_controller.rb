module Documentation
  class LanguagesController < ApplicationController

    skip_before_action :set_version

    def set_language
      if params[:locale].present? && params[:locale].to_sym.in?(@application_locales)
        session[:locale] = params[:locale]
      else
        flash[:alert] = t('documentation.alerts.no_lang')
      end
      redirect_back(fallback_location: unspecified_root_path)
    end

  end
end