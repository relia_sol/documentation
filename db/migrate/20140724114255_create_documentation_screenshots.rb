class CreateDocumentationScreenshots < ActiveRecord::Migration[4.2]
  def change
    create_table :documentation_screenshots do |t|
      t.string :alt_text
    end unless table_exists?(:documentation_screenshots)
  end
end
