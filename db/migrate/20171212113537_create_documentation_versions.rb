class CreateDocumentationVersions < ActiveRecord::Migration[4.2]
  def change
    create_table :documentation_versions do |t|
      t.string :ordinal
      t.timestamps
    end unless table_exists?(:documentation_versions)
  end
end
