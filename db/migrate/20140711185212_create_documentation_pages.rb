class CreateDocumentationPages < ActiveRecord::Migration[4.2]
  def up
    create_table "documentation_pages" do |t|
      t.string :title, :permalink, :locale
      t.text :content, :compiled_content
      t.integer :parent_id, :position, :version_id
      t.timestamps
    end unless table_exists?(:documentation_pages)
  end

  def down
    drop_table :documentation_pages
  end
end
